package com.aviva.core.services.impl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

import com.aviva.core.services.DateAndTimeService;

@Component(immediate = true)
@Service
public class DateAndTimeServiceImpl implements DateAndTimeService {

	public String getDateAndTime() {

		Date date = new Date();

		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

		return dateFormat.format(date);

	}

}
