<%--

  topnavigation component.

  This is the top navigation component

--%><%
%><%@include file="/libs/foundation/global.jsp"%><%
%><%@page session="false" %><%
%>

<%@ page import="java.util.Iterator,
        com.day.text.Text,
        com.day.cq.wcm.api.PageFilter, com.day.cq.wcm.api.Page" %><%

    // get starting point of navigation
    Page navRootPage = currentPage.getAbsoluteParent(1);
    if (navRootPage == null && currentPage != null) {
    navRootPage = currentPage;
    }
    if (navRootPage != null) {
        Iterator<Page> children = navRootPage.listChildren(new PageFilter(request));
        while (children.hasNext()) {
            Page child = children.next();
            %><a href="<%= child.getPath() %>.html">                 <%=child.getTitle() %></a><%
        }
    }
%>