<%--

  additionofnumbers component.

  This is the component for adding 2 numbers

--%><%
%><%@include file="/libs/foundation/global.jsp"%><%
%><%@page session="false" %><%
%>

	<%	
    int sumof2numbers=0;

    if(properties.get("Num1","")!= "" && properties.get("Num2","")!= "")  {

    String num1= properties.get("Num1","");

    String num2 = properties.get("Num2","");

	sumof2numbers= Integer.parseInt(num1)+Integer.parseInt(num2);
    %>
    <h1> The value of Num1: <%= properties.get("Num1","")%></h1>
	<h1> The value of Num2: <%= properties.get("Num2","")%></h1>
    <h1> The Sum of 2 Numbers is : <%=sumof2numbers%> <h1>

	<%  }
	else {
		out.print("Please provide the values for num1 and num2 by editing the component");
	}
%>